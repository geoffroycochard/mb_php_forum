<?php
// src/Controller/ForumController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ForumController extends AbstractController
{
    /**
      * @Route("/index")
      */
    public function index()
    {
        return $this->render('forum/accueil.html.twig');
    }
    /**
      * @Route("/connexion")
      */
    public function connexion()
    {
        return $this->render('user/connexion.html.twig');
    }
    /**
      * @Route("/inscription")
      */
      public function inscription()
      {
          return $this->render('user/inscription.html.twig');
      }
}